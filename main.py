#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 23 17:44:10 2018

@author: antoine
"""

# sudo apt install python3-feedparser

import os
import urllib
import feedparser
from bs4 import BeautifulSoup


# directory where images are stored
# DIR = "/home/antoine/Images/"
DIR = os.path.dirname(os.path.realpath(__file__)) + "/"
print(DIR)

class PictureOfTheDay():
    "class to represent a picture"

    def __init__(self, entrie):
        self.title = entrie.title
        self.link = entrie.link
        self.guid = entrie.guid

        html = entrie.description
        soup = BeautifulSoup(html, "lxml")

        self.img_url_low_quality = soup.find("img").get("src")
        self.img_url = self.img_url_low_quality.replace("300px", "1400px")
        self.name = soup.find("img").get("alt")
        self.filename = DIR + self.name

    def download(self):
        "to download the image"
        if not os.path.exists(DIR):
            os.makedirs(DIR)

        print("Downloading : ", self.name)
        urllib.request.urlretrieve(self.img_url, self.filename)

    def __repr__(self):
        return self.name + "\n" + self.img_url


class Pictures():
    "class to represent all the picture found on wikimedia"

    def __init__(self):
        url = "https://commons.wikimedia.org/w/api.php?action=featuredfeed&feed=potd&feedformat=rss&language=en"
        rss = feedparser.parse(url)

        self.pictures_list = []
        for entrie in rss.entries:
            potd = PictureOfTheDay(entrie)
            self.pictures_list.append(potd)

    def __repr__(self):
        txt = ""
        for pict in self.pictures_list:
            txt += "\n" + str(pict)
        return txt

    def download(self):
        "to download all the images"
        for pict in self.pictures_list:
            pict.download()
# ==================


# first , let us find all the images on wikimedia rss
PICTURES = Pictures()

# info on all the images
# print(PICTURES)
# to download all the images
# PICTURES.download()


# info on the last one
thelastone = PICTURES.pictures_list[-1]
# print(thelastone)
# to download the last one
thelastone.download()
print(thelastone.filename)

# to set the picture as wallpaper on xfce (do not work)
#import subprocess
##cmd = "xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/image-path -s " + thelastone.filename
#cmd = "xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitor0/image-path --set " + thelastone.filename
#xfce_wall_cmd = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
